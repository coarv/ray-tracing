cmake_minimum_required(VERSION 3.0.0)
project(ray-tracing VERSION 0.1.0)

include_directories(headers)
set(HEADERS headers/vec3.h headers/ray.h headers/hitable.h headers/sphere.h headers/hitable_list.h headers/camera.h headers/material.h)

add_executable(ray-tracing main.cpp ${HEADERS})

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
